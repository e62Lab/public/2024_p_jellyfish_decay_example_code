# Jellyfish degradation example code

The included juPyter notebook contains an example implementation of a bacterial population dynamics model, presented in the paper "Dynamic Population Modeling of Bacterioplankton Community Response to Gelatinous Marine Zooplankton Bloom Collapse and its Impact on Marine Nutrient Balance". The same code, with a few modifications allowing for parameter optimizations, was used in the preparation of the results for the manuscript. This code is meant to demonstrate to the reader the effects of diverse parameters on the model output.

The included JSON files contain the presets to recreate the model fits presented in the manuscript.

## Use
Quick test: open the notebook in a juPyter-supporting environment, run all cells.

Additional options can be selected, e.g., to run a preset, which replicates the <em>Aurelia aurita</em> full experiment:
* In the third cell set `runCase = True`, and `whatCase = 'Aa_full'`
* Run all cells

Upon execution, an interactive window with a graph pops-up. Beneath the graph are sliders, which can be used to manipulate the parameter values.

Code contributors:\
Filip Strniša\
Luka Jože Pušlar
